import React from 'react';
import './App.css';
import Signin from './components/Signin'
import SignUp from './components/Signup'
import {BrowserRouter as Router, Route, Link} from 'react-router-dom'

function App() {
  return (
    <div className="App">
    <Router >
    <Route exact path="/" component={Signin}/>
    <Route path="/signup" component={SignUp}/>
    </Router>
    </div>
  );
}

export default App;
